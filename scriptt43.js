$(document).ready(function(){
    $('#btn-to-top').on('click',function(){
        $(window).scrollTop(0);
        console.log('Nút to top được nhấn !!!');
        
    });
    onPageLoading()

    var gDataDrink = "";
    gPercent = 0;

    gSelectedPizzaType = "";
   
    var gPizzaSelectCombo = {
        menuName: "",    // S, M, L
        duongKinhCM: 0,
        suonNuong: 0,
        saladGr: 0,
        drink: 0,
        priceVND: 0
    }

    var gCustomerInfo = {
        menuCombo: null,
        loaiPizza: "",
        ten: "",
        email: "",
        dienThoai: "",
        diaChi: "",
        loiNhan: "",
        voucher:"",
        nuocUong:""
    };
    var gVoucherObj = "";

    $(document).on('click','#btn-small',function(){
        onBtnSmallSizeClick()
    })
    $(document).on('click','#btn-medium',function(){
        onBtnMediumSizeClick()
    })
    $(document).on('click','#btn-large',function(){
        onBtnLargeSizeClick()
    })
    $(document).on('click','#btn-sea',function(){
        onBtnSeaClick()
    })
    $(document).on('click','#btn-hawai',function(){
        onBtnHawaiClick()
    })
    $(document).on('click','#btn-bacon',function(){
        onBtnBaconClick()
    })
    
    //Hàm xử lý nút chọn loại pizza sea
    function onBtnSeaClick(){
        "use strict";
        console.log('Nút chọn loại pizza được ấn !!!')
        gSelectedPizzaType = "OCEAN MANIA";
        onChangeColorButton('sea')
        console.log('Loại pizza được chọn : ' + gSelectedPizzaType)
    }
    //Hàm xử lý nút chọn loại pizza hawai
    function onBtnHawaiClick(){
        "use strict";
        console.log('Nút chọn loại pizza được ấn !!!')
        gSelectedPizzaType = "HAWAIIAN";
        onChangeColorButton('hawai')
        console.log('Loại pizza được chọn : ' + gSelectedPizzaType)
    }
    //Hàm xử lý nút chọn loại pizza bacon
    function onBtnBaconClick(){
        "use strict";
        console.log('Nút chọn loại pizza được ấn !!!')
        gSelectedPizzaType = "CHEESY CHICKEN BACON";
        onChangeColorButton('bacon')
        console.log('Loại pizza được chọn : ' + gSelectedPizzaType)
    }

    //hàm sự chọn combo small size
    function onBtnSmallSizeClick(){
        "use strict";
        console.log('Nút combo small size được ấn !!!');
        onChangeColorButton("small")
        var vPizzaSelectCombo = getComboSelected("S",20,2,200,2,150000)
        gPizzaSelectCombo = vPizzaSelectCombo;
        console.log("Combo size được chọn là : ")
        console.log(gPizzaSelectCombo);
    }
    //hàm sự chọn combo medium size
    function onBtnMediumSizeClick(){
        "use strict";
        console.log('Nút combo small size được ấn !!!');
        onChangeColorButton("medium")
        var vPizzaSelectCombo = getComboSelected("M",25,4,300,3,200000)
        gPizzaSelectCombo = vPizzaSelectCombo;
        console.log("Combo size được chọn là : ")
        console.log(gPizzaSelectCombo);
    }
        //hàm sự chọn combo large size
    function onBtnLargeSizeClick(){
        "use strict";
        console.log('Nút combo small size được ấn !!!');
        onChangeColorButton("large")
        var vPizzaSelectCombo = getComboSelected("L",30,8,500,4,250000)
        gPizzaSelectCombo = vPizzaSelectCombo;
        console.log("Combo size được chọn là : ")
        console.log(gPizzaSelectCombo);
    }

    //hàm xử lý đổi màu nút
    function onChangeColorButton(paramSize) {
        "use strict";
        var vSmall = $("#btn-small");
        var vMedium = $("#btn-medium");
        var vLarge = $("#btn-large");
        var vHawai = $("#btn-hawai");
        var vSea = $("#btn-sea");
        var vBacon = $("#btn-bacon");

        if(paramSize == "small") {
            vSmall.removeClass('btn-warning')
            vSmall.addClass("btn-success")
            vMedium.addClass("btn-warning")
            vLarge.addClass("btn-warning")
        }

        if(paramSize == "medium") {
            vMedium.removeClass('btn-warning')
            vSmall.addClass("btn-warning")
            vMedium.addClass("btn-success")
            vLarge.addClass("btn-warning")
        }

        if(paramSize == "large") {
            vLarge.removeClass('btn-warning')
            vSmall.addClass("btn-warning")
            vMedium.addClass("btn-warning")
            vLarge.addClass("btn-success")
        }

        if(paramSize == "hawai") {
            vHawai.removeClass('btn-warning')
            vHawai.addClass("btn-success")
            vBacon.addClass("btn-warning")
            vSea.addClass("btn-warning")
        }

        if(paramSize == "sea") {
            vSea.removeClass('btn-warning')
            vSea.addClass("btn-success")
            vBacon.addClass("btn-warning")
            vHawai.addClass("btn-warning")
        }

        if(paramSize == "bacon") {
            vBacon.removeClass('btn-warning')
            vBacon.addClass("btn-success")
            vSea.addClass("btn-warning")
            vHawai.addClass("btn-warning")
        }
    }


    function getComboSelected(
        paramMenuName,
        paramDuongKinhCM,
        paramSuongNuong,
        paramSaladGr,
        paramDrink,
        paramPriceVND
      ) {
        var vSelectedMenu = {
          menuName: paramMenuName, // S, M, 
          duongKinhCM: paramDuongKinhCM,
          suonNuong: paramSuongNuong,
          saladGr: paramSaladGr,
          drink: paramDrink,
          priceVND: paramPriceVND,
        };
        return vSelectedMenu;
    }
    //Hàm load trang
    function onPageLoading(){
        "use strict";
        console.log('Tải trang !!!')
        //call api lấy data drink
        callApiToGetDataDrink();
        //đỗ dữ liệu vào select drink
        loadDataToDrinkSelect();
    }

    //Hàm đổ dữ liệu vào select drink
    function loadDataToDrinkSelect(){
        //console.log(gDataDrink)
        $.each(gDataDrink , function (i,item){
            $("#select-drink").append($("<option>" , {
                text:item.tenNuocUong,
                value:item.maNuocUong
            }))
        })
    }



    //Hàm call api lấy dữ liệu nước uống
    function callApiToGetDataDrink(){
        "use strict";
        const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
        $.ajax({
        url: vBASE_URL,
        type: 'GET',
        dataType: 'json', // added data type
        async: false,
        success: function (responseObject) {
            //console.log(JSON.stringify(responseObject));  //response text
            console.log("get data thành công!!!")
            console.log(responseObject);
            gDataDrink = responseObject
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
        });  
    }

    //Hàm sự kiện nút gửi đơn hàng
    $("#btn-send-order").on("click", function(){
        gCustomerInfo = getCustomerInfo();
        var vCheck = validateData(gCustomerInfo);
        if (vCheck == true) {
            console.log("Dữ liệu hợp lệ!");
            console.log(gCustomerInfo);
            $('#order-detail-modal').modal('show');
            showDataToModalConfirm(gCustomerInfo)
        }  
    })

    //hàm xử lý hiện thông tin đơn hàng vào modal confirm
    function showDataToModalConfirm(paramCustomerInfo){
        "use strict";
        var vThanhToan = gCustomerInfo.priceAnnualVND();
        var vDivShowData = $('#div-show-data');
        vDivShowData.html("Họ và tên: " + paramCustomerInfo.ten + "<br>");
        vDivShowData.append( "Email: " + paramCustomerInfo.email + "<br>");
        vDivShowData.append( "Địa chỉ: " + paramCustomerInfo.diaChi + "<br>");
        vDivShowData.append( "Số điện thoại : " + paramCustomerInfo.dienThoai + "<br>");
        vDivShowData.append( "....................................." + "<br>");
        vDivShowData.append( "Loại pizza : " + paramCustomerInfo.loaiPizza + "<br>");
        vDivShowData.append( "Kích cỡ: " + paramCustomerInfo.menuCombo.menuName + "<br>");
        vDivShowData.append( "Đường kính: " + paramCustomerInfo.menuCombo.duongKinhCM + "<br>");
        vDivShowData.append( "Sườn nướng: " + paramCustomerInfo.menuCombo.suonNuong + "<br>");
        vDivShowData.append( "Salad : " + paramCustomerInfo.menuCombo.saladGr + "<br>");
        vDivShowData.append( "Nước Ngọt : " + paramCustomerInfo.nuocUong + "<br>");
        vDivShowData.append( "Số Lượng Nước : " + gPizzaSelectCombo.drink + "<br>");
        vDivShowData.append( "....................................." + "<br>"); 
        vDivShowData.append( "Lời nhắn: " + paramCustomerInfo.loiNhan + "<br>");
        vDivShowData.append( "Mã voucher : " + paramCustomerInfo.voucher + "<br>");
        vDivShowData.append( "Giá VNĐ : " + paramCustomerInfo.menuCombo.priceVND + " VNĐ" + "<br>");
        vDivShowData.append( "Giảm giá : " + gPercent +" %" + "<br>");
        vDivShowData.append( "Phải thanh toán VNĐ : " + vThanhToan + " VNĐ" );
        
    }

    function validateData(paramCustomerInfo){
        "use strict";
        if (paramCustomerInfo.menuCombo.menuName === ""){
            alert("Bạn hãy chọn Menu Combo pizza!")
           return false;
          }
          if (paramCustomerInfo.loaiPizza === ""){
            alert("Bạn hãy chọn Loại pizza!")
            return false;
          }
          if (paramCustomerInfo.nuocUong === ""){
            alert("Bạn hãy chọn đồ uống!")
            return false;
          }
    
          if (paramCustomerInfo.ten === ""){
            alert("Bạn hãy nhập Họ và tên!")
            return false;
          }
          if(paramCustomerInfo.email == ""){
            alert("Bạn hãy nhập email");
            return false;
            }
            var vCheckEmail = validateEmail(paramCustomerInfo.email);
            if(vCheckEmail === false){
            return false;
            }
          
          if (paramCustomerInfo.dienThoai === ""){
            alert("Bạn hãy nhập Số điện thoại !")
            return false;
          }
          if (paramCustomerInfo.diaChi === ""){
            alert("Bạn hãy nhập địa chỉ!")
            return false;
          }
          
        return true;
     }
    
    function validateEmail(paramEmail){
        "use strict";
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var vCheckEmail = true;
        if(!mailformat.test(paramEmail)){
            alert("Email chưa hợp lệ");
            vCheckEmail = false;
        }
        return vCheckEmail;
    }

    function getCustomerInfo(){
        "use strict";
      var vInpHoVaTenValue = $("#inp-name").val().trim();
      var vInpEmailValue = $("#inp-email").val().trim();
      var vInpDienThoaiValue = $("#inp-phone").val().trim();
      var vInpDiaChiValue = $("#inp-address").val().trim();
      var vInpLoiNhanValue = $("#inp-message").val().trim();
      var vInpVoucherValue = $("#inp-voucher").val().trim();
      var vInpDrinkValue = $("#select-drink option:selected").val();

      var vCustomerInfo = {
            menuCombo: gPizzaSelectCombo,
            loaiPizza: gSelectedPizzaType,
            nuocUong: vInpDrinkValue,
            ten: vInpHoVaTenValue,
            email: vInpEmailValue,
            dienThoai:vInpDienThoaiValue,
            diaChi: vInpDiaChiValue,
            loiNhan: vInpLoiNhanValue,
            voucher: vInpVoucherValue,
            priceAnnualVND: function () {
                // goi ham tinh phan tram
                if(vInpVoucherValue == ""){
                    gPercent = 0
                    var vTotal = this.menuCombo.priceVND * (1 - gPercent / 100);
                    return vTotal;
                }else{ 
                    callApiToGetVoucher(vInpVoucherValue);
                    console.log(gVoucherObj);
                    var vTotal2 = this.menuCombo.priceVND * (1 - gPercent / 100);
                    return vTotal2;
                }
            }
        }
      return vCustomerInfo;
    }

    function callApiToGetVoucher(paramVoucherId){
        "use strict";
        const vBASE_URL = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
        $.ajax({
          url: vBASE_URL + paramVoucherId,
          async: false,
          type: "GET",
          dataType: 'json',
          success: function(responseObject){
            console.log(responseObject);
            gVoucherObj = responseObject;
            gPercent = responseObject.phanTramGiamGia;
        },
            error: function(error){
            gPercent = 0;

          }
      })
      }

    
  $(document).on('click','#btn-create-order',function(){
    var vOrderId = "";
    var vObjectRequest = {
      kichCo: gCustomerInfo.menuCombo.menuName,
      duongKinh: gCustomerInfo.menuCombo.duongKinhCM,
      suon: gCustomerInfo.menuCombo.suonNuong,
      salad: gCustomerInfo.menuCombo.saladGr,
      loaiPizza: gCustomerInfo.loaiPizza,
      idVourcher: gCustomerInfo.voucher,
      idLoaiNuocUong: gCustomerInfo.nuocUong,
      soLuongNuoc: gCustomerInfo.menuCombo.drink,
      hoTen: gCustomerInfo.ten,
      thanhTien: gCustomerInfo.priceAnnualVND(),
      email: gCustomerInfo.email,
      soDienThoai: gCustomerInfo.dienThoai,
      diaChi: gCustomerInfo.diaChi,
      loiNhan: gCustomerInfo.loiNhan
  }
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      async: false,
      type: "POST",
      data: JSON.stringify(vObjectRequest),
      contentType: "application/json;charset=UTF-8",
      success: function(responseObject){
        console.log(responseObject);
        vOrderId = responseObject.orderId;
      },
        error: function(error){
        alert(error.responseText);
      }
  })
    $("#order-detail-modal").modal("hide");
    $("#order-id-modal").modal("show");
    $("#inp-modal-order-id").val(vOrderId);
  })
})

$(document).on('click','#btn-end',function(){
    location.reload();
    $(window).scrollTop(0);
})
